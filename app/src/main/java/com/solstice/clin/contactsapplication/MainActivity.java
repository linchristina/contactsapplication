package com.solstice.clin.contactsapplication;

/**
 * Christina Lin
 * Solstice Mobile New Hire Evaluation Project
 *
 * MainActivity of the app, handles the http requests and reading of the JSON data
 * Uses the Volley library to process the JSON requests
 * Iterates through the JSON array and parses the JSON objects
 * Extracts the contact information and stores it in a Contact object
 * After the data is retrieved and parsed, it displays the contacts in a listview
 *
 * When a contact is clicked on, it starts another activity
 * The contact's information is sent to the second activity via an Intent
 * This second activity retrieves the information and lists the individual contact's information
 *
 */

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends Activity {

    // log tag
    private static final String TAG = MainActivity.class.getSimpleName();

    // contacts json url
    private static final String url = "https://solstice.applauncher.com/external/contacts.json";
    private ProgressDialog pdialog;
    private List<Contact> contactList = new ArrayList<Contact>();
    private ListView listView;
    private ListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listView = (ListView) findViewById(R.id.list);
        adapter = new ListAdapter(this, contactList);
        listView.setAdapter(adapter);

        pdialog = new ProgressDialog(this);
        // Showing progress dialog to user before making http request
        pdialog.setMessage("Loading...");
        pdialog.show();

        // Creating volley request object
        JsonArrayRequest req = new JsonArrayRequest(url,
            new Response.Listener<JSONArray>() {
                @Override
                public void onResponse(JSONArray response) {
                    Log.d(TAG, response.toString());
                    hidePDialog();

                    // Iterate through JSON array and parse json
                    for (int i = 0; i < response.length(); i++) {
                        try {
                            JSONObject obj = response.getJSONObject(i);
                            final Contact contact = new Contact();
                            contact.setName(obj.getString("name"));
                            contact.setEmployeeId(obj.getString("employeeId"));
                            contact.setCompany(obj.getString("company"));
                            contact.setDetailsURL(obj.getString("detailsURL"));

                            // Request json from detailsURL
                            JsonObjectRequest detailObj = new JsonObjectRequest(Request.Method.GET, contact.getDetailsURL(), null,
                                new Response.Listener<JSONObject>() {
                                    @Override
                                    public void onResponse(JSONObject obj) {
                                        for (int j = 0; j < obj.length(); j++) {
                                            try {
                                                contact.setFavorite(obj.getString("favorite"));
                                                contact.setLargeImageURL(obj.getString("largeImageURL"));
                                                contact.setEmail(obj.getString("email"));
                                                contact.setWebsite(obj.getString("website"));

                                                // getting address
                                                JSONObject addressObj = obj.getJSONObject("address");
                                                ArrayList<String> address = new ArrayList();
                                                address.add(addressObj.getString("street"));
                                                address.add(addressObj.getString("city"));
                                                address.add(addressObj.getString("state"));
                                                address.add(addressObj.getString("country"));
                                                address.add(addressObj.getString("zip"));
                                                address.add(addressObj.getString("latitude"));
                                                address.add(addressObj.getString("longitude"));

                                                contact.setAddress(address);
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }
                                        }

                                    }
                                }, new Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {
                                        VolleyLog.d(TAG, "Error: " + error.getMessage());
                                    }
                                }
                            );
                            // Adding detailObj request to queue
                            VolleyController.getInstance().addToRequestQueue(detailObj);

                            contact.setSmallImageURL(obj.getString("smallImageURL"));
                            contact.setBirthdate(obj.getString("birthdate"));

                            // getting phone  numbers
                            JSONObject phoneObj = obj.getJSONObject("phone");
                            ArrayList<String> phone = new ArrayList<String>();
                            phone.add((String) phoneObj.getString("work"));
                            phone.add((String) phoneObj.getString("home"));
                            phone.add((String) phoneObj.getString("mobile"));
                            contact.setPhone(phone);

                            // add contact to contacts arraylist
                            contactList.add(contact);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    // notify list adapter about data changes
                    // so it can render the listview with updated data
                    adapter.notifyDataSetChanged();
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    VolleyLog.d(TAG, "Error: " + volleyError.getMessage());
                    hidePDialog();
                }
            }
        );

        // Add request to request queue
        VolleyController.getInstance().addToRequestQueue(req);

        // Checking if user clicked on a contact
        // If a contact was clicked, starts a second activity and
        // sends the contact information to that second activity
        ListView list = (ListView) findViewById(R.id.list);
        list.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                String name = ((TextView) view.findViewById(R.id.name)).getText().toString();

                // Starting single contact activity
                Intent in = new Intent(getApplicationContext(), SingleContactActivity.class);
                in.putExtra("name", contactList.get(position).getName());
                in.putExtra("employeeId", contactList.get(position).getEmployeeId());
                in.putExtra("company", contactList.get(position).getCompany());
                in.putExtra("birthdate", contactList.get(position).getBirthdate());
                in.putExtra("phone_work", contactList.get(position).getPhone().get(0));
                in.putExtra("phone_home", contactList.get(position).getPhone().get(1));
                in.putExtra("phone_mobile", contactList.get(position).getPhone().get(2));

                in.putExtra("favorite", contactList.get(position).getFavorite());
                in.putExtra("largeImageURL", contactList.get(position).getLargeImageURL());
                in.putExtra("email", contactList.get(position).getEmail());
                in.putExtra("website", contactList.get(position).getWebsite());
                in.putExtra("address_street", contactList.get(position).getAddress().get(0));
                in.putExtra("address_city", contactList.get(position).getAddress().get(1));
                in.putExtra("address_state", contactList.get(position).getAddress().get(2));
                in.putExtra("address_country", contactList.get(position).getAddress().get(3));
                in.putExtra("address_zip", contactList.get(position).getAddress().get(4));
                in.putExtra("address_latitude", contactList.get(position).getAddress().get(5));
                in.putExtra("address_longitude", contactList.get(position).getAddress().get(6));
                startActivity(in);
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        hidePDialog();
    }

    private void hidePDialog() {
        if (pdialog != null) {
            pdialog.dismiss();
            pdialog = null;
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

}
