package com.solstice.clin.contactsapplication;

/**
 * Christina Lin
 * Solstice Mobile New Hire Evaluation Project
 *
 * Provides data to fill the contact list with
 *
 */

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;

import java.util.List;

public class ListAdapter extends BaseAdapter {
    private Activity activity;
    private LayoutInflater inflater;
    private List<Contact> contactItems;
    ImageLoader imgloader = VolleyController.getInstance().getImageLoader();

    public ListAdapter(Activity activity, List<Contact> contactItems) {
        this.activity = activity;
        this.contactItems = contactItems;
    }

    @Override
    public int getCount() {
        return contactItems.size();
    }

    @Override
    public Object getItem(int location) {
        return contactItems.get(location);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (inflater == null) {
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.list_row, null);
        }
        if (imgloader == null) {
            imgloader = VolleyController.getInstance().getImageLoader();
        }

        NetworkImageView smallImage = (NetworkImageView) convertView.findViewById(R.id.smallImage);
        TextView name = (TextView) convertView.findViewById(R.id.name);

        // getting contact data for row
        Contact c = contactItems.get(position);

        // smallImage image
        smallImage.setImageUrl(c.getSmallImageURL(), imgloader);

        // name
        name.setText(c.getName());

        return convertView;
    }
}
