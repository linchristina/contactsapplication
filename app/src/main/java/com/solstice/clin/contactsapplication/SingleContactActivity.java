package com.solstice.clin.contactsapplication;

/**
 * Christina Lin
 * Solstice Mobile New Hire Evaluation Project
 *
 * Activity is started when user clicks on one of the contacts
 * Retrieves the contact information from the previous intent
 * and displays it on the screen
 *
 */

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.CheckBox;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;


public class SingleContactActivity extends Activity {

    private TextView vName;
    private TextView vEmployeeId;
    private TextView vCompany;
    private TextView vBirthdate;

    private CheckBox vFavorite;
    private NetworkImageView vLargeImage;
    private TextView vEmail;
    private TextView vWebsite;

    ImageLoader imgloader = VolleyController.getInstance().getImageLoader();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_contact);

        // Get JSON values from previous intent
        Intent in = getIntent();
        String name = in.getStringExtra("name");
        String employeeId = in.getStringExtra("employeeId");
        String company = in.getStringExtra("company");
        String birthdate = in.getStringExtra("birthdate");
        String phone_work = in.getStringExtra("phone_work");
        String phone_home = in.getStringExtra("phone_home");
        String phone_mobile = in.getStringExtra("phone_mobile");

        String favorite = in.getStringExtra("favorite");
        String largeImageURL = in.getStringExtra("largeImageURL");
        String email = in.getStringExtra("email");
        String website = in.getStringExtra("website");
        String address_street = in.getStringExtra("address_street");
        String address_city = in.getStringExtra("address_city");
        String address_state = in.getStringExtra("address_state");
        String address_country = in.getStringExtra("address_country");
        String address_zip = in.getStringExtra("address_zip");
        String address_latitude = in.getStringExtra("address_latitude");
        String address_longitude = in.getStringExtra("address_longitude");

        // Display values on the screen
        vLargeImage = (NetworkImageView) findViewById(R.id.c_largeImage);
        vLargeImage.setImageUrl(largeImageURL, imgloader);

        vName = (TextView) findViewById(R.id.c_name);
        vName.setText(name);

        vFavorite = (CheckBox) findViewById(R.id.c_favorite);
        if (favorite.equalsIgnoreCase("true")) {
            vFavorite.setChecked(true);
        }

        vEmployeeId = (TextView) findViewById(R.id.c_id);
        vEmployeeId.setText("ID: " + employeeId);

        vCompany = (TextView) findViewById(R.id.c_company);
        vCompany.setText("Company\n" + company);

        TextView vPhone = (TextView) findViewById(R.id.c_phone);
        vPhone.setText("Phone\n"
            + phone_work + "\t(Work)\n"
            + phone_home + "\t(Home)\n"
            + phone_mobile + "\t(Mobile)");

        vBirthdate = (TextView) findViewById(R.id.c_birthdate);
        vBirthdate.setText("Birth Date\n" + birthdate);

        vEmail = (TextView) findViewById(R.id.c_email);
        vEmail.setText("Email\n" + email);

        TextView vAddr = (TextView) findViewById(R.id.c_addr);
        vAddr.setText("Address\n" + address_street + "\n"
            + address_city + ", " + address_state +  " " + address_zip + "\n"
            + address_country + "\n"
            + "Latitude: " + address_latitude + "\n"
            + "Longitude: " + address_longitude);

        vWebsite = (TextView) findViewById(R.id.c_website);
        vWebsite.setText("Website\n" + website);
    }

}
