package com.solstice.clin.contactsapplication;

/**
 * Christina Lin
 * Solstice Mobile New Hire Evaluation Project
 *
 * This class is for storing the contact information retrieved from the JSON data
 *
 */

import java.util.ArrayList;

public class Contact {
    // Variables
    private String name;
    private String employeeId;
    private String company;
    private String detailsURL;
    private String smallImageURL;
    private String birthdate;
    private ArrayList<String> phone;

    private String favorite;
    private String largeImageURL;
    private String email;
    private String website;
    private ArrayList<String> address;

    // Constructors
    public Contact() {}

    public Contact(String name, String employeeId, String company,
                   String detailsURL, String smallImageURL,
                   String birthdate, ArrayList<String> phone,
                   String favorite, String largeImageURL, String email,
                   String website, ArrayList<String> address) {
        this.name = name;
        this.employeeId = employeeId;
        this.company = company;
        this.detailsURL = detailsURL;
        this.smallImageURL = smallImageURL;
        this.birthdate = birthdate;
        this.phone = phone;
        this.favorite = favorite;
        this.largeImageURL = largeImageURL;
        this.email = email;
        this.website = website;
        this.address = address;
    }

    // Getters and setters
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getEmployeeId() {
        return employeeId;
    }
    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }
    public String getCompany() {
        return company;
    }
    public void setCompany(String company) {
        this.company = company;
    }
    public String getDetailsURL() {
        return detailsURL;
    }
    public void setDetailsURL(String detailsURL) {
        this.detailsURL = detailsURL;
    }
    public String getSmallImageURL() {
        return smallImageURL;
    }
    public void setSmallImageURL(String smallImageURL) {
        this.smallImageURL = smallImageURL;
    }
    public String getBirthdate() {
        return birthdate;
    }
    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }
    public ArrayList<String> getPhone() {
        return phone;
    }
    public void setPhone(ArrayList<String> phone) {
        this.phone = phone;
    }

    public String getFavorite() {
        return favorite;
    }
    public void setFavorite(String favorite) {
        this.favorite = favorite;
    }
    public String getLargeImageURL() {
        return largeImageURL;
    }
    public void setLargeImageURL(String largeImageURL) {
        this.largeImageURL = largeImageURL;
    }
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public String getWebsite() {
        return website;
    }
    public void setWebsite(String website) {
        this.website = website;
    }
    public ArrayList<String> getAddress() {
        return address;
    }
    public void setAddress(ArrayList<String> address) {
        this.address = address;
    }
}
