package com.solstice.clin.contactsapplication;

/**
 * Christina Lin
 * Solstice Mobile New Hire Evaluation Project
 *
 * Initializes core objects of volley library
 * Volley library assists in http requests and parsing the JSON data
 *
 */

import android.app.Application;
import android.text.TextUtils;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;

public class VolleyController extends Application {

    public static final String TAG = VolleyController.class.getSimpleName();

    private RequestQueue queue;
    private ImageLoader imgloader;

    private static VolleyController instance;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
    }

    public static synchronized VolleyController getInstance() {
        return  instance;
    }

    public RequestQueue getRequestQueue() {
        if (queue == null) {
            queue = Volley.newRequestQueue(getApplicationContext());
        }
        return queue;
    }

    public ImageLoader getImageLoader() {
        getRequestQueue();
        if (imgloader == null) {
            imgloader = new ImageLoader(this.queue, new ImgCache());
        }
        return this.imgloader;
    }

    public <T> void addToRequestQueue(Request<T> req, String tag) {
        // set the default tag if the tag is empty
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }

    public void cancelPendingRequests(Object tag) {
        if (queue != null) {
            queue.cancelAll(tag);
        }
    }
}
